# bash prompt

# red prompt if exit status is not 0
PS1="\e[0;32m\]\u@\h\e[m\] \W \$(if [[ \$? == 0 ]]; then echo \"\e[0;36m\]\"; else echo \"\e[0;31m\]\"; fi)\$\e[m\] "
