#!/bin/sh

# Install configuration files.
# This script symlinks config files into the home directory.
# usage: ./setup.sh

ln -svi "$PWD"/.xinitrc ~
ln -svi "$PWD"/.Xresources ~

ln -svi "$PWD"/.i3 ~
ln -svi "$PWD"/.conkyrc ~

ln -svi "$PWD"/.inputrc ~

[ ! -d ~/.bash ] && mkdir ~/.bash
ln -svi "$PWD"/.bashrc ~
ln -svi "$PWD"/.bash/*.sh ~/.bash

ln -svi "$PWD"/.bash_it/themes/custom/custom.theme.bash ~/.bash_it/themes/custom/

ln -svi "$PWD"/.vimrc ~
ln -svi "$PWD"/.vim ~

ln -svi "$PWD"/.ncmpcpp ~

ln -svi "$PWD"/.gitconfig ~
