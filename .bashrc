#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Environment variables
export EDITOR="vim"
export PATH="${PATH}:$HOME/bin"

# Tab completion
complete -cf sudo
complete -cf man
complete -cf pidof 
complete -cf killall 

# Load other stuff including prompt and aliases
for f in ~/.bash/*.sh; do
  source "$f"
done

# Bash-it config
export BASH_IT=`echo ~/.bash_it`
export BASH_IT_THEME='custom'
unset MAILCHECK
export IRC_CLIENT='weechat'
export SCM_CHECK=true
source $BASH_IT/bash_it.sh

# Unlimited history
HISTSIZE=
HISTFILESIZE=
